# How to run

Enable Virtual Environment


### Linux

```bash
source ../bin/activate
```

### Windows

```bash
cd ../Scripts
activate
cd CHATBOTAGRO
```

## Running Server

Run below code and open http://127.0.0.1:8000 in your browser

```bash

pip install -r requirements.txt
python manage.py runserver
```

## Training

Run below code to train the bot

```bash
./train.sh
```

If you see *PermissionError* or run as administrator in windows cmd

```bash
chmod 777 train.sh
```

## Screen Shot

![Screenshot](/Screenshots/chatbot.png)