"""CHATBOTAGRO URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

from rest_framework import routers

from chat.views import HomePage

from data.apis import CropViewSet
from chat.apis import ChatSessionViewSet, MessageViewSet
from chatbot.views import ChatResponse

router = routers.DefaultRouter()
router.register(r'crop', CropViewSet)

router.register(r'c_session', ChatSessionViewSet)
router.register(r'message', MessageViewSet)

urlpatterns = [
    path('', HomePage.as_view()),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('chatbot/', ChatResponse.as_view()),
]
