from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from rasa_core.agent import Agent

from rasa_core.interpreter import RasaNLUInterpreter
import json

interpreter = RasaNLUInterpreter('chatbot/models/current/nlu')

strt_msg = "Hi! you can chat in this window. Type '/stop' to end the conversation."
#agent = Agent.load('chatbot/models/dialogue', interpreter=interpreter)

class ChatResponse(View):
    def get(self, request):
        agent = Agent.load('chatbot/models/dialogue', interpreter=interpreter)
        responses = agent.handle_message(request.GET.get('message'))
        if not responses:
            response = {"text": ""}
        else:
            response = responses[0]
        return JsonResponse(response, safe=False)
