## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:mood_affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:mood_deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad

## intent:query_wheat
- which is the best season to grow wheat?
- when should i start cultivating wheat?
- Is this good season to grow wheat?

## intent:query_rice_soil
- What kind of soil is best suited for rice?
- what soil is suited for rice?
- can rice be grown in soil?

## intent: query_rice_season
- which is the best season to grow rice?
- when should i start cultivating rice?
- Is this good season to grow rice?
- which is the best season to grow paddy?
- when should i start cultivating paddy?
- Is this good season to grow paddy?

## intent: query_rice_varieties
- How many varieties of rice are there?
- How many rice categories are there?
- How many kinds of rice are there?

## intent: query_rice_yields
- What is the expected yeild of rice in ..?
- How many tons of rice can be grown in .. acres?

## intent: query_groundnuts_season
- Best season /climate to grow peanuts 
- When do we grow Peanuts?
- When do peanuts germinate the best?

## intent: query_groundnuts_temperature
- Best temperature to grow peanuts 
- At what degree peanuts grow the best?
- Most suitable temperature for peanuts to grow ?

## intent: query_groundnuts_water
- Amount of rainfall required to grow peanuts
- How much mm rainfall required to grow peanuts?
- Minimum rainfall required to grow peanuts?

## intent query_maize_soil
- What should be the soil conditions to get good yield of maize?
- In which soil maize grows the best?
- What is the Optimum soil  condition for maize to grow?

## intent query_maize_season
- What season of the year does maize grow?
- In which season do we get best yield in maize?

## intent query_maize_water
- Amount of rainfall required to grow Maize?
- What is the rainfall in millimeters to grow maize?

