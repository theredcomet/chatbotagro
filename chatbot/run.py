
from rasa_core.agent import Agent
from rasa_core.interpreter import RasaNLUInterpreter
import json

interpreter = RasaNLUInterpreter('models/current/nlu')

strt_msg = "Hi! you can chat in this window. Type '/stop' to end the conversation."
agent = Agent.load('models/dialogue', interpreter=interpreter)


print(strt_msg)
while True:
    a = input()
    if a == '/stop':
        print("closing the console!")
        break
    responses = agent.handle_message(a)
    result = interpreter.parse(a)
    print(json.dumps(result, indent=4, sort_keys=True))
    for r in responses:
        print(r)

