"""
Implements REST API's

-   Uses djangorestframework and url_filter

"""

from rest_framework import serializers, viewsets

from .models import Crop

class CropSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Crop
        fields = ('id', 'crop_name')

class CropViewSet(viewsets.ModelViewSet):
    queryset = Crop.objects.all()
    serializer_class = CropSerializers