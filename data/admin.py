from django.contrib import admin

from .models import Crop

class CropAdmin(admin.ModelAdmin):
    pass

admin.site.register(Crop, CropAdmin)
