from django.db import models

class Crop(models.Model):
    """
    Describes CROP
    """

    crop_name = models.CharField(
        max_length=100,
        verbose_name="Crop Name",
        help_text="Ensure less than 100 Characters")
    
    def clean(self):
        self.crop_name = self.crop_name.strip()

    def __str__(self):
        return self.crop_name


