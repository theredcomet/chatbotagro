from django.shortcuts import render
from django.views import View

class HomePage(View):
    def get(self, request):
        template_name = "chat/index.html"
        context = dict()
        return render(request, template_name, context)
