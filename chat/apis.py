"""
Implements REST API's

-   Uses djangorestframework and url_filter

"""

from rest_framework import serializers, viewsets

from .models import ChatSession, Message

class ChatSessionSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ChatSession
        fields = ('id', 'created_on', 'modified_on')

class ChatSessionViewSet(viewsets.ModelViewSet):
    queryset = ChatSession.objects.all()
    serializer_class = ChatSessionSerializers


class MessageSessionSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'chat_session', 'created_on', 'sent_by', 'text')

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSessionSerializers