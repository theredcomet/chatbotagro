from django.db import models
import uuid

class ChatSession(models.Model):
    """
    Chat Session
    -   Stores uuid, used to recognize unique chat sessions
    -   Can be connected to unique Users (django or custom User model or Visitor Model)
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.id)

class Message(models.Model):
    """
    Message:

    -   Stores messages as text
    -   connected to ChatSession (OneToOne)
    -   Sent By marking
    -   Supports only text messages as of now.

    *Can be enhanced with Visitor/User model by adding multi-user chat session or bots.*
    """
    SENT_BY = (
        (0, "BOT"),
        (1, "USER"),
    )

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)

    sent_by = models.IntegerField(default=0, choices=SENT_BY)
    text = models.TextField()
    chat_session = models.ForeignKey(ChatSession, on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.id)