python -m rasa_core.train -d chatbot/domain.yml -s chatbot/stories.md -o chatbot/models/dialogue
python -m rasa_nlu.train -c chatbot/nlu_config.yml --data chatbot/nlu.md -o chatbot/models --fixed_model_name nlu --project current --verbose
